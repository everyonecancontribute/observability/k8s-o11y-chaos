# Kubernetes Observability with Chaos Engineering

Demo project for Michael Friedrich's Observability talks. It deploys applications into a connected Kubernetes cluster in Civo Cloud, and defines alerts, SLOs to trigger them using Chaos experiments.

_Avatar credit: [Photo](https://unsplash.com/photos/OA0qcP6GOw0) by <a href="https://unsplash.com/@etiennegirardet?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Etienne Girardet</a> on <a href="https://unsplash.com/s/photos/chaos?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>_
  

## Introduction

This project combines demos and practical ideas for the talks

- "Confidence with Chaos for your Kubernetes Observability"
  - [Abstract](https://docs.google.com/document/d/1Ecy9oBQh6POvi0qcCJFzcDFAS_IBj0omkL_aEO41Fbk/edit)
  - [Slides from Continuous Lifecycle](https://docs.google.com/presentation/d/12GyltfC2oadnx5POVGkkd_JNGpsaD4FY4KF72F-Dqtc/edit)
  - [Recording from Oben Observability Day at KubeCon NA](https://www.youtube.com/watch?v=g13DTh8qDpk)
- "From Monitoring to Observability: Left Shift your SLOs with Chaos" 
  - [Abstract](https://docs.google.com/document/d/1ku-Tp0mBHsWNiY_BwvQoE1Dxlp8BdR5MYy-sJhJotQs/edit#)
  - [Slides from Kubernetes Community Days Munich](https://docs.google.com/presentation/d/16jy_QtiMGCFIcswU8wxmt4WOb8fqreg3fCRJctiPs-s/edit)
  - [Recording from KubeCon EU](https://youtube.com/watch?v=BkREMg8adaI)

The project is based on the KubeCon EU demo project [cpp-dns-leaker](https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker) and follow-up discussions and ideas. The application leaks memory but only when DNS requests against a specificed domain are failing.  

This project uses the [GitLab agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/) to integrate a Civo Cloud Kubernetes cluster where additional deployments and demo commands are being added. 

### Technologies

- [Prometheus](https://prometheus.io/) and [Prometheus Operator](https://prometheus-operator.dev/)
- [Jaeger Tracing](https://www.jaegertracing.io/)
- [OpenTelemetry](https://opentelemetry.io/)
- [Chaos Mesh](https://chaos-mesh.org/)
- [Kubernetes](https://kubernetes.io/), in [Civo Cloud](https://www.civo.com/)
- [GitLab](https://about.gitlab.com/)

#### Learning resources

- [Metrics](https://o11y.love/topics/metrics/)
- [Tracing](https://o11y.love/topics/tracing/)
- [OpenTelemetry](https://o11y.love/topics/collections-specs/#opentelemetry)
- [Chaos Engineering](https://o11y.love/topics/chaos-engineering/)

### Feedback and ideas 

- Kubernetes drift detection in combination with chaos
- How often should chaos experiments run in a pipeline to have reproducible results
- Continuous Chaos Engineering and reliability 

Blog series: https://gitlab.com/gitlab-com/marketing/community-relations/dev-evangelism/meta/-/issues/121 

### Overview 

1. Deployment in Civo, using the GitLab agent for Kubernetes 
1. Metrics best practices in Kubernetes
1. Alerts and SLOs for infrastructure and application metrics (alerts to GitLab HTTP endpoint too)
1. Chaos experiment with pod failure (kubedoom as a fun demo too)
1. Chaos experiment with DNS and [cpp-dns-leaker](https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker) from KubeCon EU
1. [Nginx instrumented with OpenTelemetry webserver SDK service deployment](https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry) and Chaos experiment with stress tests 

Ideas: 

1. OpenTelemetry instrumented app and long running backend requests (**TODO**)

## License

All files and configuration snippets are available under the MIT license. See exceptions below. 

Exceptions: 

- [kubernetes/kubedoom](kubernetes/kubedoom) is a Git subtree that is licensed under GPLv3
 

## Demo 

### Demo Web Interfaces

```
# Prometheus
kubectl --namespace monitoring port-forward svc/prometheus-k8s 9090

# Grafana
kubectl --namespace monitoring port-forward svc/grafana 3000 

# Chaos Mesh
kubectl --namespace chaos-testing port-forward svc/chaos-dashboard 2333

# Alert Manager
kubectl --namespace monitoring port-forward svc/alertmanager-main 9093

# Jaeger Tracing
kubectl --namespace observability port-forward service/simplest-query 16686
```

| Description | Tool | URL |
|-------------|------|-----|
| Container RSS Memory | Prometheus | http://localhost:9090/graph?g0.expr=container_memory_rss%7Bcontainer%3D~%22cpp-dns-leaker-service.*%22%7D&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=30m |
| o11y chaos dashboard | Grafana | http://localhost:3000/d/R4Vz5hq7z/o11y-chaos-dashboard |
| Chaos Schedules | Chaos Mesh | http://localhost:2333/schedules/ |
| Alerts | Alert Manager | http://localhost:9093/#/alerts |
| Alert Receiver | GitLab | https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/alert_management |
| Tracing UI | Jaeger | http://localhost:16686/ |

### Demo: Chaos Schedule with DNS failures 

Open Chaos Mesh Schedules and upload the [kubernetes/chaos/dns-schedule.yml](kubernetes/chaos/dns-schedule.yml) YAML configuration, submit two times and run the schedule.

Attach to a pod in the `default` namespace. 

```
kubectl get pods

kubectl logs -f cpp-dns-leader-o11y-*
```

### Demo: Chaos Schedule with Stress Testing Nginx OTel service

Open Chaos Mesh Schedules and upload the [kubernetes/chaos/stress-cpu-mem-nginx-otel.yml](kubernetes/chaos/stress-cpu-mem-nginx-otel.yml) YAML configuration, submit two times adn run the schedule. Alternatively, start the existing schedule. 

Open Jaeger Tracing at http://localhost:16686/ and show the different trace and span times. 

Optionally: Attach to the curl pod logs.

```
kubectl logs -f pod/testcurl
```

### Demo: Cleanup

Restart the `default` namespace deployments after leaking memory, etc. 

```
kubectl -n default rollout restart deploy
```


### Game: Kubedoom

Forward port 5900 from the pod.

```
kubectl get pods

kubectl port-forward kubedoom-6f8c65fd85-52fx2 5900:5900 -n kubedoom
```

Open VNC Viewer, skip without login, and connect to `localhost:5900`.

Use the password `idbehold`.

Cheats (if you need them):

- iddqd (god mode)
- idkfa (give everything)
- idspispopd (clip through the wall to the monsters with the pod names)


More hints in https://opensource.com/article/21/6/kube-doom 

## Deployments

### my-kube-prometheus

Apply changes and customizations the same way.

```
cd kubernetes/o11y/my-kube-prometheus

vim my-prom.jsonnet

docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci jb update
docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci ./build.sh my-prom.jsonnet

```

### Alerts

```
kubectl apply -f kubernetes/o11y/my-kube-prometheus/alerts/*.yaml
```

### App: C++ DNS Leaker

Deployed with the GitLab agent in [kubernetes/o11y/cpp-dns-leaker](kubernetes/o11y/cpp-dns-leaker).

### App: Nginx with OpenTeleetry 

Using [nginx-opentelemetry](https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry) together with the OpenTelemtry and Jaeger deployment. 


### App: Rust Web app with OpenTelemetry tracing

TODO: Implement with rocket.rs


## GitLab Integration

Agent for Kubernetes (see [requirements](#requirements)) enables additional security and observability features. Some features requires an Ultimate subscription.

### Troubleshooting

https://docs.gitlab.com/ee/user/clusters/agent/troubleshooting.html

```
kubectl logs -f -l=app=gitlab-agent -n gitlab-agent
```

### Alerts

https://docs.gitlab.com/ee/operations/incident_management/alerts.html

`Settings > Monitor > Alerts`, created `k8s-prometheus` and copied the URL and auth key into [kubernetes/o11y/my-kube-prometheus/alertmanager-config.yaml]

### Operational Container Scanning

https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html

```
vim .gitlab/agents/ecc-o11y-chaos.yaml

starboard:
  cadence: '0 0 * * *' # Daily at 00:00 (Kubernetes cluster time)
```

## Development

### Kubedoom

Kubedoom is added as Git subtree from https://github.com/storax/kubedoom 
```
git subtree add --prefix kubernetes/kubedoom https://github.com/storax/kubedoom.git HEAD --squas
h
```

## Requirements

Note: This is already prepared for this project.

### Civo k3s cluster

Install the Civo CLI and create the cluster.

```
civo kubernetes create ecc-o11y-chaos
civo kubernetes config ecc-o11y-chaos --save
kubectl config use-context ecc-o11y-chaos
kubectl get node
```

### ingress-nginx

```
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```

### Kubedoom

```
kubectl apply -k kubernetes/kubedoom/manifest
```

Install VNC viewer, e.g. as Homebrew cask. 

```
brew install vnc-viewer
```


### Chaos Mesh

```
helm repo add chaos-mesh https://charts.chaos-mesh.org

helm search repo chaos-mesh

kubectl create ns chaos-testing

helm install chaos-mesh chaos-mesh/chaos-mesh -n=chaos-testing

helm upgrade --install chaos-mesh chaos-mesh/chaos-mesh \ 
--namespace=chaos-testing \
--set dnsServer.create=true \
--set dashboard.create=true \
--set dashboard.securityMode=false \
--set chaosDaemon.hostNetwork=true \
--set chaosDaemon.runtime=containerd \
--set chaosDaemon.socketPath=/run/k3s/containerd/containerd.sock \
--reuse-values


kubectl get pods -n chaos-testing -l app.kubernetes.io/component=chaos-dns-server

kubectl --namespace chaos-testing port-forward svc/chaos-dashboard 2333
```

### OpenTelemetry and Jaeger Tracing

Inspired by https://knative.dev/blog/articles/distributed-tracing/ with own adjustments. 

* [cert-manager](https://cert-manager.io/docs/installation/) (required)
* [Jaeger Operator](https://github.com/jaegertracing/jaeger-operator#getting-started)
* [OpenTelemetry Operator](https://github.com/open-telemetry/opentelemetry-operator#getting-started)

```
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.1/cert-manager.yaml

kubectl create namespace observability

kubectl create -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.34.1/jaeger-operator.yaml -n observability 

kubectl apply -f https://github.com/open-telemetry/opentelemetry-operator/releases/latest/download/opentelemetry-operator.yaml
```

Create a Jaeger pod.

```
kubectl -n observability apply -f https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/raw/main/kubernetes/o11y/jaeger/simplest.yaml
```

```
kubectl -n observability port-forward service/simplest-query 16686
```

http://localhost:16686/ 

Configure the OpenTelemetry collector.

```
kubectl apply -f https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/raw/main/kubernetes/o11y/opentelemetry/collector.yaml 
```

Verify that the OpenTelemetry collector is properly setup:

```
kubectl describe OpenTelemetryCollector -n observability
```


##### Nginx OpenTelemetry container

Deploy the Nginx container with the OpenTelemetry webserver SDK module pre-installed and pre-configured for Otel/Jaeger for traces. 

```
kubectl apply -f https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry/-/raw/main/kubernetes/manifests/nginx-otel-service.yml 

+kubectl logs -f service/nginx-otel-service
```

Find the correct hostname to send the curl requests to.

```
kubectl describe service/nginx-otel-service

kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml

kubectl exec -i -t dnsutils -- nslookup nginx-otel-service.default.svc.cluster.local
```


Deploy a curl image and curl the Nginx container every X seconds. 

```
kubectl apply -f https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry/-/raw/main/kubernetes/manifests/nginx-otel-service.yml

kubectl create -f https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry/-/raw/main/kubernetes/manifests/curl.yml 
```

Note: This container modifies the command, and needs to be deleted and created. 

Add port forwarding for Jaeger UI, and inspect the second service. 

```
kubectl -n observability port-forward service/simplest-query 16686
```


### kube-prometheus

A customized version from [kubernetes/o11y/my-kube-prometheus](kubernetes/o11y/my-kube-prometheus) is deployed.

```
kubectl apply --server-side -f kubernetes/o11y/my-kube-prometheus/manifests/setup

kubectl apply -f kubernetes/o11y/my-kube-prometheus/manifests
```


### GitLab Agent for Kubernetes

Follow the [documentation](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html) to install the agent.

```
mkdir .gitlab/agents/ecc-o11y-chaos
touch .gitlab/agents/ecc-o11y-chaos/config.yaml
```

Register the agent in the Kubernetes cluster with GitLab.

`Infrastructure > Kubernetes Clusters` and click on `Connect a cluster (agent)` and select the agent name.

Copy the instructions and install the agent.

```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install ecc-o11y-chaos gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.1.0 \
    --set config.token=XXX \
    --set config.kasAddress=wss://kas.gitlab.com \
    --set serviceAccount.name=gitlab-agent 
```


https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/cluster_agents/ecc-o11y-chaos

#### Upgrade

Inspect the latest release and override the value to upgrade. 

```
helm repo update
helm upgrade --install ecc-o11y-chaos gitlab/gitlab-agent \
  --namespace gitlab-agent \
  --reuse-values \
   --set image.tag=v15.3.0
```  
