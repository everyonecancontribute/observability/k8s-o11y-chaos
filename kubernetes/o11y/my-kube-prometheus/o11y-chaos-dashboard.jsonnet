local grafana = import 'grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local row = grafana.row;
local prometheus = grafana.prometheus;
local template = grafana.template;
local graphPanel = grafana.graphPanel;

{
  'o11y-chaos-dashboard.json':
    dashboard.new('o11y Chaos Dashboard')
    .addTemplate(
      {
        current: {
          text: 'Prometheus',
          value: 'Prometheus',
        },
        hide: 0,
        label: null,
        name: 'datasource',
        options: [],
        query: 'prometheus',
        refresh: 1,
        regex: '',
        type: 'datasource',
      },
    )
    .addRow(
      row.new()
      .addPanel(graphPanel.new('Container Memory RSS', span=6, datasource='$datasource')
                .addTarget(prometheus.target('container_memory_rss{container=~"cpp-dns-leaker-service.*"}')))
    ),
}
